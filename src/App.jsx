import Calculator from './features/calc/Calculator.jsx';
import {Box, Container, CssBaseline, ThemeProvider} from "@mui/material";
import {Provider} from "react-redux";
import store from './app/store.js';
import theme from './theme';

const App = () => (
    <Container maxWidth="sm">
        <Box sx={{my: 4}}>
            <Calculator/>
        </Box>
    </Container>
);

const SimpleCalculator = () => (
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <CssBaseline/>
            <App/>
        </ThemeProvider>
    </Provider>
);

export default {App, SimpleCalculator};

