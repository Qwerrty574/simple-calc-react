import {configureStore} from '@reduxjs/toolkit';
import {calculatorReducer} from '../features/calc';

const store = configureStore({
    reducer: {
        calculator: calculatorReducer,
    },
});

export default store;
