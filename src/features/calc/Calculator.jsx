import {useDispatch, useSelector} from 'react-redux';
import {inputDigit, inputDot, handleOperator, resetCalculator, deleteLast} from './calculatorSlice.js';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import {styled} from '@mui/material/styles';

const CalculatorContainer = styled('div')({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
});

const ButtonsContainer = styled('div')({
    display: 'grid',
    gridTemplateColumns: 'repeat(4, 1fr)',
    gap: '10px',
    marginTop: '20px',
});

const Calculator = () => {
    const dispatch = useDispatch();
    const {displayValue, error} = useSelector((state) => state.calculator);

    const handleClick = (content) => {
        if (content === 'C') {
            dispatch(resetCalculator());
        } else if (content === 'DEL') {
            dispatch(deleteLast());
        } else if (['+', '-', '*', '/', '='].includes(content)) {
            dispatch(handleOperator(content));
        } else if (content === '.') {
            dispatch(inputDot());
        } else {
            dispatch(inputDigit(content));
        }
    };

    const buttons = [
        '7', '8', '9', '/',
        '4', '5', '6', '*',
        '1', '2', '3', '-',
        '0', '.', '=', '+',
        'DEL', 'C'
    ];

    return (
        <CalculatorContainer>
            <TextField
                label="Display"
                value={displayValue}
                InputProps={{readOnly: true}}
                variant="outlined"
                error={Boolean(error)}
                multiline
            />
            <ButtonsContainer>
                {buttons.map((content) => (
                    <Button
                        key={content}
                        variant="outlined"
                        onClick={() => handleClick(content)}
                    >
                        {content}
                    </Button>
                ))}
            </ButtonsContainer>
        </CalculatorContainer>
    );
};

export default Calculator;
