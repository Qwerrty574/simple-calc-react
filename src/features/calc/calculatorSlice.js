import {createSlice} from '@reduxjs/toolkit';

const initialState = {
    displayValue: '0',
    firstOperand: null,
    waitingForSecondOperand: false,
    operator: null,
    error: null
};

const calculatorSlice = createSlice({
    name: 'calculator',
    initialState,
    reducers: {
        inputDigit(state, action) {
            const {displayValue, waitingForSecondOperand} = state;
            const digit = action.payload;

            if (waitingForSecondOperand) {
                state.displayValue = digit;
                state.waitingForSecondOperand = false;
            } else {
                state.displayValue = displayValue === '0' ? digit : displayValue + digit;
            }
        },
        inputDot(state) {
            if (!state.displayValue.includes('.')) {
                state.displayValue += '.';
            }
        },
        handleOperator(state, action) {
            const {firstOperand, displayValue, operator} = state;
            const inputValue = parseFloat(displayValue);
            const newOperator = action.payload;

            if (state.error) {
                state.error = null;
                state.displayValue = '0';
                state.firstOperand = null;
                state.waitingForSecondOperand = false;
                state.operator = null;
                return;
            }

            if (operator && state.waitingForSecondOperand) {
                state.operator = newOperator;
                return;
            }

            if (firstOperand == null) {
                state.firstOperand = inputValue;
            } else if (operator) {
                const result = performCalculation[operator](firstOperand, inputValue);
                state.displayValue = String(result);
                state.firstOperand = result;

                if (isFinite(result)) {
                    state.displayValue = String(result);
                    state.firstOperand = result;
                } else {
                    if (inputValue === 0) {
                        state.displayValue = 'Error: Zero Division';
                    } else {
                        state.displayValue = 'Error: Overflow';
                    }
                    state.error = true;
                    return;
                }
            }

            state.waitingForSecondOperand = true;
            state.operator = newOperator;
        },
        resetCalculator(state) {
            state.displayValue = '0';
            state.firstOperand = null;
            state.waitingForSecondOperand = false;
            state.operator = null;
            state.error = null;
        },
        deleteLast(state) {
            if (state.error) {
                state.displayValue = '0';
                state.error = null;
                return;
            }
            if (state.waitingForSecondOperand === false) {
                state.displayValue = state.displayValue.length > 1 ? state.displayValue.slice(0, -1) : '0';
            }
        }
    },
});

const performCalculation = {
    '/': (firstOperand, secondOperand) => firstOperand / secondOperand,
    '*': (firstOperand, secondOperand) => firstOperand * secondOperand,
    '+': (firstOperand, secondOperand) => firstOperand + secondOperand,
    '-': (firstOperand, secondOperand) => firstOperand - secondOperand,
    '=': (firstOperand, secondOperand) => secondOperand,
};

export const {
    inputDigit,
    inputDot,
    handleOperator,
    resetCalculator,
    deleteLast
} = calculatorSlice.actions;

export default calculatorSlice.reducer;
