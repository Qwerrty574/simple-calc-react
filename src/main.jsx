import React from 'react';
import {createRoot} from "react-dom/client";
import SimpleCalculator from "./App";

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
    <React.StrictMode>
        <SimpleCalculator/>
    </React.StrictMode>,
)




