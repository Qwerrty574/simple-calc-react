// noinspection JSUnusedGlobalSymbols

import {defineConfig} from 'vite';
import react from '@vitejs/plugin-react';
import {resolve} from 'path'

export default defineConfig({
    plugins: [react()],
    esbuild: {
        loader: 'jsx', // Ensure esbuild uses the jsx loader for .js files
        include: /\.(js|jsx)$/, // Apply loader to both .js and .jsx files
    },
    build: {
        lib: {
            entry: resolve(__dirname, 'src/App.jsx'),
            name: 'App',
            fileName: (format) => `simple-calculator.${format}.js`
        },
        rollupOptions: {
            // Внешние зависимости, которые не нужно включать в сборку
            external: ['react', 'react-dom', '@mui/material', '@emotion/react', '@emotion/styled', 'react-redux', '@reduxjs/toolkit'],
            output: {
                globals: {
                    react: 'React',
                    'react-dom': 'ReactDOM',
                    '@mui/material': 'MaterialUI',
                    '@emotion/react': 'emotionReact',
                    '@emotion/styled': 'emotionStyled',
                    'react-redux': 'ReactRedux',
                    '@reduxjs/toolkit': 'RTK'
                }
            }
        }
    }
});

