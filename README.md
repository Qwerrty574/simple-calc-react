![React Version](https://img.shields.io/badge/React-18.3.3-blue)
![App Version](https://img.shields.io/badge/version-1.0.0-brightgreen)
# Simple calculator on React

## Getting started

Simple calculator on React for УИТИА

## Installation
Use the package manager npm to install requirements.

```bash
npm install
```

## Usage
```bash
npm run dev
```

## Authors and acknowledgment
<img src="https://secure.gravatar.com/avatar/565a4bc6e45717c7c6a622963d4e737f19aab30b09de886bdaac083b94c6518a?s=80&d=identicon" height="20px" width="20px" alt="favicon"/> [Qwerrty574](https://gitlab.com/Qwerrty574)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Project status
In progress
